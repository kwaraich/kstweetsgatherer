﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using TweetsGatherer.Domain;
using TweetsGatherer.Services;
using TweetsGatherer.Tasks;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.Tests.Tasks
{
    [TestFixture]
    public class TweetsTaskTests
    {
        private ITweetsTask _task;
        private Mock<IAppSettings> _appSettingsMock;
        private Mock<ITwitterAuthorizationService> _twitterAuthorizationServiceMock;
        private Mock<ITweetsSearchService> _tweetsSearchServiceMock;

        private string _account1;
        private string _account2;

        private string _twitterConsumerKeySettingKey;
        private string _twitterConsumerKey;
        private string _twitterConsumerSecretSettingKey;
        private string _twitterConsumerSecret;
        private string _twitterAccountsSettingKey;
        private string _twitterAccounts;
        private string _numberOfDaysSettingKey;
        private int _numberOfDays;

        private string _tweet11Content;
        private string _tweet12Content;
        private string _tweet21Content;

        private IList<Tweet> _tweetsForAccount1; 
        private IList<Tweet> _tweetsForAccount2; 

        private string _accessToken;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            _account1 = "account1";
            _account2 = "account2";

            _twitterConsumerKeySettingKey = "twitter:ConsumerKey";
            _twitterConsumerKey = "consumerKey";
            _twitterConsumerSecretSettingKey = "twitter:ConsumerSecret";
            _twitterConsumerSecret = "consumerSecret";
            _twitterAccountsSettingKey = "TwitterAccounts";
            _twitterAccounts = _account1 + "," + _account2;
            _numberOfDaysSettingKey = "PastNumberOfDaysToGrabTweets";
            _numberOfDays = 14;

            _accessToken = "accessToken";

            _tweet11Content = "content_tweet_1_account_1";
            _tweet12Content = "content_tweet_2_account_1";
            _tweet21Content = "content_tweet_1_account_2";

            _tweetsForAccount1 = new List<Tweet>
            {
                new Tweet
                {
                    Content = _tweet11Content,
                    DateAndTime = DateTime.Now.AddDays(-1),
                    NumberOfUsersMentioned = 5
                },
                new Tweet
                {
                    Content = _tweet12Content,
                    DateAndTime = DateTime.Now.AddDays(-3),
                    NumberOfUsersMentioned = 2
                }
            };
            _tweetsForAccount2 = new List<Tweet>
            {
                new Tweet
                {
                    Content = _tweet21Content,
                    DateAndTime = DateTime.Now.AddDays(-2),
                    NumberOfUsersMentioned = 1
                }
            };
        }

        [SetUp]
        public void SetUp()
        {
            _appSettingsMock = new Mock<IAppSettings>();
            _twitterAuthorizationServiceMock = new Mock<ITwitterAuthorizationService>();
            _tweetsSearchServiceMock = new Mock<ITweetsSearchService>();

            _appSettingsMock.Setup(x => x.Get(_twitterConsumerKeySettingKey)).Returns(_twitterConsumerKey);
            _appSettingsMock.Setup(x => x.Get(_twitterConsumerSecretSettingKey)).Returns(_twitterConsumerSecret);
            _appSettingsMock.Setup(x => x.Get(_twitterAccountsSettingKey)).Returns(_twitterAccounts);
            _appSettingsMock.Setup(x => x.Get(_numberOfDaysSettingKey)).Returns(_numberOfDays.ToString);

            _twitterAuthorizationServiceMock.Setup(
                x => x.GetApplicationOnlyAccessToken(_twitterConsumerKey, _twitterConsumerSecret)).Returns(_accessToken);

            _tweetsSearchServiceMock.Setup(x => x.GetTweets("account1", _numberOfDays, _accessToken))
                .Returns(_tweetsForAccount1);
            _tweetsSearchServiceMock.Setup(x => x.GetTweets("account2", _numberOfDays, _accessToken))
                .Returns(_tweetsForAccount2);

            _task = new TweetsTask(_appSettingsMock.Object, _twitterAuthorizationServiceMock.Object, _tweetsSearchServiceMock.Object);
        }

        [Test]
        public void GetTweets_ShouldGet_AppSettings()
        {
            _task.GetTweets();

            _appSettingsMock.Verify(x => x.Get(_twitterConsumerKeySettingKey));
            _appSettingsMock.Verify(x => x.Get(_twitterConsumerSecretSettingKey));
            _appSettingsMock.Verify(x => x.Get(_twitterAccountsSettingKey));
            _appSettingsMock.Verify(x => x.Get(_numberOfDaysSettingKey));
        }

        [Test]
        public void GetTweets_Should_AquireTokenFrom_AuthorizationService()
        {
            _task.GetTweets();
            _twitterAuthorizationServiceMock.Verify(x => x.GetApplicationOnlyAccessToken(_twitterConsumerKey, _twitterConsumerSecret));
        }

        [TestCase("account1")]
        [TestCase("account2")]
        public void GetTweets_Should_GetTweetsFrom_SearchService_ForeachAccount(string account)
        {
            _task.GetTweets();
            _tweetsSearchServiceMock.Verify(x => x.GetTweets(account, _numberOfDays, _accessToken));
        }

        [Test]
        public void GetTweets_ReturnsProperAggregateInfo()
        {
            var result = _task.GetTweets();

            Assert.AreEqual(_account1, result.AccountsAggregateInformation[0].Account);
            Assert.AreEqual(2, result.AccountsAggregateInformation[0].NumberOfTweets);
            Assert.AreEqual(7, result.AccountsAggregateInformation[0].NumberOfUsersMentionedInTweets);
            Assert.AreEqual("The @account1 tweeted 2 times, in which 7 users were mentioned", result.AccountsAggregateInformation[0].Info);

            Assert.AreEqual(_account2, result.AccountsAggregateInformation[1].Account);
            Assert.AreEqual(1, result.AccountsAggregateInformation[1].NumberOfTweets);
            Assert.AreEqual(1, result.AccountsAggregateInformation[1].NumberOfUsersMentionedInTweets);
            Assert.AreEqual("The @account2 tweeted 1 times, in which 1 users were mentioned", result.AccountsAggregateInformation[1].Info);
        }

        [Test]
        public void GetTweets_ReturnsTweetsSortedByDate()
        {
            var result = _task.GetTweets();
            Assert.AreEqual(3, result.TweetsData.Count());
            Assert.AreEqual(_tweet12Content, result.TweetsData[0].Tweet);
            Assert.AreEqual(_tweet21Content, result.TweetsData[1].Tweet);
            Assert.AreEqual(_tweet11Content, result.TweetsData[2].Tweet);
        }
    }
}
