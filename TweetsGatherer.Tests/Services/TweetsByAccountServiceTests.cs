﻿using System;
using System.Net;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using TweetsGatherer.Dtos.Twitter;
using TweetsGatherer.Exceptions;
using TweetsGatherer.Services;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.Tests.Services
{
    [TestFixture]
    public class TweetsByAccountServiceTests
    {
        private ITweetsSearchService _service;
        private Mock<IAppSettings> _appSettingsMock;
        private Mock<IRestClient> _restClientMock;

        private string _searchEndpointSettingKey;
        private string _searchEndpoint;
        private string _accessToken;
        private string _account;
        private int _numberOfDays;
        private string _twitterSearchResponseContent;
        private IRestResponse _twitterSearchResponse;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            _searchEndpointSettingKey = "twitter:SearchApiEndpoint";
            _searchEndpoint = "https//:mock.twittersearch.endpoint";
            _accessToken = "mock_token";
            _account = "mock_account";
            _numberOfDays = 7;


            _twitterSearchResponseContent =
                JsonConvert.SerializeObject(new SearchResponseDto
                {
                    statuses =
                        new[]
                        {
                            new SearchResponseTweetDto
                            {
                                text = "text1",
                                created_at = "Sat Apr 25 20:36:02 +0000 2015",
                                entities =
                                    new SearchResponseEntitiesDto
                                    {
                                        user_mentions =
                                            new[]
                                            {
                                                new SearchResponseUserDto
                                                {
                                                    name = "Karandeep Singh",
                                                    screen_name = "karandeep"
                                                }
                                            }
                                    }
                            },
                            new SearchResponseTweetDto
                            {
                                text = "text2",
                                created_at = "Sat Apr 25 18:36:02 +0000 2015",
                                entities =
                                    new SearchResponseEntitiesDto
                                    {
                                        user_mentions =
                                            new[]
                                            {
                                                new SearchResponseUserDto
                                                {
                                                    name = "Karandeep Singh",
                                                    screen_name = "karandeep"
                                                },
                                                new SearchResponseUserDto
                                                {
                                                    name = "Karandeep Singh 2",
                                                    screen_name = "karandeep2"
                                                },

                                            }
                                    }
                            },
                        }
                });

            _twitterSearchResponse = new RestResponse { StatusCode = HttpStatusCode.OK, Content = _twitterSearchResponseContent };
        }

        [SetUp]
        public void SetUp()
        {
            _appSettingsMock = new Mock<IAppSettings>();
            _restClientMock = new Mock<IRestClient>();

            _appSettingsMock.Setup(x => x.Get(_searchEndpointSettingKey)).Returns(_searchEndpoint);
            _restClientMock.Setup(
                x =>
                    x.Execute(
                        It.Is<IRestRequest>(r => r.Resource.Equals(_searchEndpoint) && r.Method.Equals(Method.GET))))
                .Returns(_twitterSearchResponse);

            _service = new TweetsSearchService(_appSettingsMock.Object, _restClientMock.Object);
        }

        [Test]
        public void Ctor_ShouldGet_SearchEndpointFrom_AppSettings()
        {
            _appSettingsMock.Verify(x => x.Get(_searchEndpointSettingKey));
        }


        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Parameter cannot be null or empty\r\nParameter name: account")]
        public void GetTweets_ThrowsArgumentException_WhenAccountParameter_IsInvalid(string account)
        {
            _service.GetTweets(account, _numberOfDays, _accessToken);
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Parameter cannot be null or empty\r\nParameter name: accessToken")]
        public void GetTweets_ThrowsArgumentException_WhenAccessTokenParameter_IsInvalid(string accessToken)
        {
            _service.GetTweets(_account, _numberOfDays, accessToken);
        }

        [TestCase(0)]
        [TestCase(-2)]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Parameter value cannot be less than 1\r\nParameter name: numberOfDays")]
        public void GetTweets_ThrowsArgumentException_WhenNumberOfDaysParameter_IsInvalid(int numberOfDays)
        {
            _service.GetTweets(_account, numberOfDays, _accessToken);
        }

        [Test]
        public void GetTweets_ShouldCallExecute_InRestClient_WithProperEndpoint()
        {
            _service.GetTweets(_account, _numberOfDays, _accessToken);
            _restClientMock.Verify(x => x.Execute(It.Is<IRestRequest>(r => r.Resource.Equals(_searchEndpoint))));
        }

        [Test]
        public void GetTweets_ShouldCallExecute_InRestClient_WithProper_Method()
        {
            _service.GetTweets(_account, _numberOfDays, _accessToken);
            _restClientMock.Verify(x => x.Execute(It.Is<IRestRequest>(r => r.Method.Equals(Method.GET))));
        }

        [Test]
        public void GetTweets_ShouldCallExecute_InRestClient_WithProper_AccessToken()
        {
            _service.GetTweets(_account, _numberOfDays, _accessToken); 
            _restClientMock.Verify(x => x.Execute(It.Is<IRestRequest>(r => r.Parameters.Find(p => p.Name.Equals("Authorization")).Value.Equals("Bearer {0}".With(_accessToken)))));
        
        }

        [Test]
        public void GetTweets_ShouldCallExecute_InRestClient_WithProper_SearchQuery()
        {
            var expectedSearchQuery = "from:{0} since:{1:yyyy-MM-dd}".With(_account, DateTime.UtcNow.AddDays(-(_numberOfDays)));

            _service.GetTweets(_account, _numberOfDays, _accessToken); 
            _restClientMock.Verify(x => x.Execute(It.Is<IRestRequest>(r => r.Parameters.Find(p => p.Name.Equals("q")).Value.Equals(expectedSearchQuery))));      
        }

        [Test]
        [ExpectedException(typeof(TwitterSearchException))]
        public void GetTweets_ThrowsTwitterSearchException_WhenSearchFails()
        {
            _restClientMock.Setup(
                x =>
                    x.Execute(
                        It.Is<IRestRequest>(r => r.Resource.Equals(_searchEndpoint) && r.Method.Equals(Method.GET))))
                .Throws(new Exception("SearchFailed"));
            _service.GetTweets(_account, _numberOfDays, _accessToken); 
        }

        [Test]
        public void GetTweets_ReturnsProperData()
        {
            var result = _service.GetTweets(_account, _numberOfDays, _accessToken); 

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("text1",result[0].Content);
            Assert.AreEqual("text2",result[1].Content);
            Assert.AreEqual(1, result[0].NumberOfUsersMentioned);
            Assert.AreEqual(2, result[1].NumberOfUsersMentioned);
        }

    }
}
