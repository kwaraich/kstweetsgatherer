﻿using System;
using System.Net;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using TweetsGatherer.Dtos.Twitter;
using TweetsGatherer.Exceptions;
using TweetsGatherer.Services;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.Tests.Services
{
    [TestFixture]
    public class TwitterAuthorizationServiceTests
    {
        private ITwitterAuthorizationService _service;

        private Mock<IAppSettings> _appSettingsMock;
        private Mock<IRestClient> _restClientMock;

        private string _accessToken;
        private string _consumerKey;
        private string _consumerSecret;
        private string _twitterAuthEndpoint;
        private string _twitterAuthEndpointSettingKey;
        private IRestResponse _twitterAuthResponse;
        private string _twitterAuthResponseContent;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            _accessToken = "mock_token";
            _consumerKey = "mockConsumerKey";
            _consumerSecret = "mockConsumerSecret";
            _twitterAuthEndpoint = "http//:mock.twitterauth.endpoint";
            _twitterAuthEndpointSettingKey = "twitter:ApplicationAuthEndpoint";

            _twitterAuthResponseContent =
                JsonConvert.SerializeObject(new AuthTokenResponseDto
                {
                    token_type = "bearer",
                    access_token = _accessToken
                });

            _twitterAuthResponse = new RestResponse{StatusCode = HttpStatusCode.OK,Content = _twitterAuthResponseContent};
        }

        [SetUp]
        public void SetUp()
        {
            _appSettingsMock = new Mock<IAppSettings>();
            _restClientMock = new Mock<IRestClient>();


            _appSettingsMock.Setup(x => x.Get(_twitterAuthEndpointSettingKey)).Returns(_twitterAuthEndpoint);
            _restClientMock.Setup(
                x =>
                    x.Execute(
                        It.Is<IRestRequest>(r => r.Resource.Equals(_twitterAuthEndpoint) && r.Method.Equals(Method.POST))))
                .Returns(_twitterAuthResponse);

            _service = new TwitterAuthorizationService(_appSettingsMock.Object, _restClientMock.Object);
        }

        [Test]
        public void Ctor_GetsAuthEndpoint_FromAppSettings()
        {
            _appSettingsMock.Verify(x=>x.Get(_twitterAuthEndpointSettingKey), Times.Once());
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Parameter cannot be null or empty\r\nParameter name: consumerKey")]
        public void GetApplicationOnlyAccessToken_ThrowsArgumentException_WhenConsumerKeyParameterIsInvalid(string consumerKey)
        {
            _service.GetApplicationOnlyAccessToken(consumerKey, _consumerSecret);
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Parameter cannot be null or empty\r\nParameter name: consumerSecret")]
        public void GetApplicationOnlyAccessToken_ThrowsArgumentException_WhenConsumerSecretParameterIsInvalid(string consumerSecret)
        {
            _service.GetApplicationOnlyAccessToken(_consumerKey, consumerSecret);
        }

        [Test]
        public void GetApplicationOnlyAccessToken_ShouldCallExecute_InRestClient_With_ProperEndpoint()
        {
            _service.GetApplicationOnlyAccessToken(_consumerKey, _consumerSecret);
            _restClientMock.Verify(x=>x.Execute(It.Is<IRestRequest>(r=>r.Resource.Equals(_twitterAuthEndpoint))));
        }

        [Test]
        public void GetApplicationOnlyAccessToken_ShouldCallExecute_InRestClient_With_POST_Method()
        {
            _service.GetApplicationOnlyAccessToken(_consumerKey, _consumerSecret);
            _restClientMock.Verify(x=>x.Execute(It.Is<IRestRequest>(r=>r.Method.Equals(Method.POST))));
        }

        [Test]
        public void GetApplicationOnlyAccessToken_ShouldCallExecute_InRestClient_With_ProperEncodedAuthorizationHeaderValue()
        {
            var expectedAuthorizationHeaderValue =
                "Basic {0}".With("{0}:{1}".With(_consumerKey, _consumerSecret).Base64Encode());

            _service.GetApplicationOnlyAccessToken(_consumerKey, _consumerSecret);
            _restClientMock.Verify(x=>x.Execute(It.Is<IRestRequest>(r=>r.Parameters.Find(p=>p.Name.Equals("Authorization")).Value.Equals(expectedAuthorizationHeaderValue))));
        }

        [Test]
        [ExpectedException(typeof(TwitterAuthorizationException))]
        public void GetApplicationOnlyAccessToken_ThrowsTwitterAuthorizationException_WhenRestCallUnSuccessfull()
        {
            _restClientMock.Setup(
                x =>
                    x.Execute(
                        It.Is<IRestRequest>(r => r.Resource.Equals(_twitterAuthEndpoint) && r.Method.Equals(Method.POST))))
                .Returns(new RestResponse{StatusCode = HttpStatusCode.Forbidden});

            _service.GetApplicationOnlyAccessToken(_consumerKey, _consumerSecret);
        }

        [Test]
        [ExpectedException(typeof(TwitterAuthorizationException))]
        public void GetApplicationOnlyAccessToken_ThrowsTwitterAuthorizationException_WhenRestCallThrowsException()
        {
            _restClientMock.Setup(
                x =>
                    x.Execute(
                        It.Is<IRestRequest>(r => r.Resource.Equals(_twitterAuthEndpoint) && r.Method.Equals(Method.POST))))
                .Throws(new Exception());

            _service.GetApplicationOnlyAccessToken(_consumerKey, _consumerSecret);
        }

        [Test]
        public void GetApplicationOnlyAccesToken_ReturnsProperToken()
        {
            var result = _service.GetApplicationOnlyAccessToken(_consumerKey, _consumerSecret);
            Assert.IsNotNull(result);
            Assert.AreEqual(_accessToken, result);
        }
    }
}
