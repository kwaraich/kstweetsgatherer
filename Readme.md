### Overview ###

Write a web application that gathers data from some twitter feeds, merges the feeds to a list, provides some simple aggregates, and then returns a response.

- Please use a 3rd party library for OAuth integration to the twitter API
- Do not use a 3rd third party library for integrating with twitter - write code to connect directly with Twitter API


### Implementation ###

1. Connect to the Twitter API (you will need a dev account for this)
2. Get a list of all tweets from the last two weeks for the following three twitter accounts: @pay_by_phone @PayByPhone @PayByPhone_UK
3. Return the list in a json formatted response: sorted by time, with item data: Twitter account, tweet, tweet date and time
4. Provide the following aggregate information in the response as well:

* Total number of tweets per account for the two week period
* Total number of times another user was mentioned in tweets, per account for the two week period
* (For example that the @pay_by_phone tweeted 25 times, in which 8 users were mentioned)


### Requirements ###

You are free to use any development environment, tools, and resources that you see fit.
Your solution should, however, fulfill the following requirements:

- Use C# and the .NET framework (which version is up to you)
- Create a web application, using an MVC or REST framework
- Provide a UAT test that exercises the result, as well as the original source code for both
- Focus on SOLID, unit tests, and code organization
- Code quality will be evaluated as if it were ready for release to production.



__________________________________________________________________________________________________________________________________________________


# IMPLEMENTATION DETAILS #

## All the code was developed in a git respository so that all the history and timely progrecan be reviewed later ##
## https://bitbucket.org/kwaraich/kstweetsgatherer ##

* ### Web application : ASP.Net Web Api 2.2
* ### Language        : C# / .Net 4.5
* ### Solution        : main.sln


## Notes ##

- The WebAPi supports both XML and JSON for response data, please specify "accept" header with value "application/json" in request to get JSON response.

- The application reads following configurations from the web.config app settings section :

	1. Twitter Consumer Key & Consumer Secret (By the time I make the repository public I will revoke my credentials for security reasons, please your own Key and Secret)
	2. Accounts for which the tweets are to be gathered (comma seperated values without space)
	3. How many recent days worth of tweets are to be gathered (Currently set to 14 i.e. 2 weeks)
	4. Api endpoints
 
- All the listed 3rd party twitter libraries provided more than just authorization, I chose to implement the authentication just becuase it was simple enough and gave me better understanding of how twitter token exchange works.
- The Application uses ATTRIBUTE ROUTING for route configuration.
- Structuremap has been configured for Dependency Injection / IoC
- All the domain logic is in TweetsGatherer project and the functionality  id exposed by Task layer
- The web application TweetsGatherer.API is a very light weight front end that depends on TweetsGatherer.
- All the logic was implemented using TDD 
- Project uses Nunit for testing framework and Moq for mocking.


## USER ACCEPTACE TESTS ##

* ### Solution : uat.sln ###

- user acceptance test specifications are written and implemented using SpecFlow (cucumber for .net)