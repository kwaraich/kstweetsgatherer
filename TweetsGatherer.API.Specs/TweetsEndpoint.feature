﻿Feature: TweetsEndpoint
	In order to get tweets data and information from specific users for past two weeks
	As a RESTful API  user
	I want to recieve data in json format that contains tweets ordered by date and also contains some aggregate information for each account

Scenario: The GET request to .../tweets resource
	Given I have a GET request with "http://localhost:1643/tweets" resource
	And I have specified header accept : "application/json"
	When I execute the request
	Then the response status code is 200 OK
	And the response content-type is "application/json"
	And the response content is valid json
	And the response content contains aggregate information for "pay_by_phone"
	And the response content contains aggregate information for "PayByPhone"
	And the response content contains aggregate information for "PayByPhone_UK"
	And the response content contains tweets data ordered by date & time
	
