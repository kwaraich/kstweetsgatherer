﻿using System;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;

namespace TweetsGatherer.API.Specs
{
    [Binding]
    public class TweetsEndpointSteps
    {
        private IRestClient _restClient;
        private IRestRequest _restRequest;
        private IRestResponse _restResponse;
        private JObject _jObject;

        [Given(@"I have a GET request with ""(.*)"" resource")]
        public void GivenIHaveGetRequestWithResource(string resourceUri)
        {
            _restClient = new RestClient(resourceUri);
            _restRequest = new RestRequest(Method.GET);
        }

        [Given(@"I have specified header accept : ""(.*)""")]
        public void GivenIHaveSpecifiedHeaderAccept(string acceptHeaderValue)
        {
            _restRequest.AddHeader("accept", acceptHeaderValue);
        }

        [When(@"I execute the request")]
        public void WhenIExecuteTheRequest()
        {
            _restResponse = _restClient.Execute(_restRequest);
        }

        [Then(@"the response status code is 200 OK")]
        public void ThenTheResponseStatusCodeIsOk()
        {
            Assert.AreEqual(HttpStatusCode.OK, _restResponse.StatusCode);
        }

        [Then(@"the response content-type is ""(.*)""")]
        public void ThenTheResponseContent_TypeIs(string contentType)
        {
            Assert.IsTrue(_restResponse.ContentType.Contains(contentType));
        }

        [Then(@"the response content is valid json")]
        public void ThenTheResponseContentIsValidJson()
        {
            _jObject = JObject.Parse(_restResponse.Content);
        }

        [Then(@"the response content contains aggregate information for ""(.*)""")]
        public void ThenTheResponseContentContainsAggregateInformationFor(string account)
        {
            Assert.AreEqual(3, _jObject["AccountsAggregateInformation"].Children().Count());
            var accounts = _jObject["AccountsAggregateInformation"].Children()["Account"].Values<string>();
            CollectionAssert.Contains(accounts, account);
        }

        [Then(@"the response content contains tweets data ordered by date & time")]
        public void ThenTheResponseContentContainsTweetsDataOrderedByDateTime()
        {
            var tweetDates = _jObject["TweetsData"].Children()["TweetDateAndTime"].Values<DateTime>().ToList();

            CollectionAssert.IsOrdered(tweetDates);
        }
    }
}
