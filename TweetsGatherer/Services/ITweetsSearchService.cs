﻿using System.Collections.Generic;
using TweetsGatherer.Domain;

namespace TweetsGatherer.Services
{
    public interface ITweetsSearchService : IService
    {
        /// <summary>
        /// Gets a list of tweets for {account} for last {numberOfDays} days
        /// </summary>
        /// <param name="account">Twitter account Id</param>
        /// <param name="numberOfDays">Number of Days</param>
        /// <param name="accessToken">A valid twitter application-only acces token</param>
        IList<Tweet> GetTweets(string account, int numberOfDays, string accessToken);
    }
}
