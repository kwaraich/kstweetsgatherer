﻿namespace TweetsGatherer.Services
{
    public interface ITwitterAuthorizationService : IService
    {
        string GetApplicationOnlyAccessToken(string consumerKey, string consumerSecret);
    }
}
