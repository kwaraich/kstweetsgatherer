﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using TweetsGatherer.Domain;
using TweetsGatherer.Dtos.Twitter;
using TweetsGatherer.Exceptions;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.Services
{

    public class TweetsSearchService : ITweetsSearchService
    {
        private const string SearchEndpointSettingKey = "twitter:SearchApiEndpoint";
        private readonly string _searchEndpoint;

        private readonly IRestClient _restClient;

        public TweetsSearchService(IAppSettings appSettings, IRestClient restClient)
        {
            _restClient = restClient;
            _searchEndpoint = appSettings.Get(SearchEndpointSettingKey);
        }

        public IList<Tweet> GetTweets(string account, int numberOfDays, string accessToken)
        {
            if (string.IsNullOrWhiteSpace(account))
                throw new ArgumentException("Parameter cannot be null or empty", "account");

            if (numberOfDays < 1)
                throw new ArgumentException("Parameter value cannot be less than 1", "numberOfDays");

            if (string.IsNullOrWhiteSpace(accessToken))
                throw new ArgumentException("Parameter cannot be null or empty", "accessToken");

            var date = DateTime.UtcNow.AddDays(-(numberOfDays));

            var searchQuery = "from:{0} since:{1:yyyy-MM-dd}".With(account, date);

            var request = new RestRequest(_searchEndpoint, Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("Authorization", "Bearer {0}".With(accessToken));
            request.AddQueryParameter("q", searchQuery);
            
            SearchResponseDto searchResponse;

            try
            {
                var response = _restClient.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    searchResponse = JsonConvert.DeserializeObject<SearchResponseDto>(response.Content);
                }
                else
                    throw new Exception("Twitter search api response status code : {0} [Content: {1}]".With(response.StatusCode, response.Content));
            }
            catch (Exception ex)
            {
                throw new TwitterSearchException("Twitter search failed.", ex);
            }

            return searchResponse.statuses.Select(tweetStatus => new Tweet { Content = tweetStatus.text, DateAndTime = DateTime.ParseExact(tweetStatus.created_at, "ddd MMM dd HH:mm:ss zzz yyyy", CultureInfo.InvariantCulture), NumberOfUsersMentioned = tweetStatus.entities.user_mentions.Count() }).ToList();
        }
    }
}
