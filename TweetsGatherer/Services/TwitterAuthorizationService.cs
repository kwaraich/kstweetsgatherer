﻿using System;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using TweetsGatherer.Dtos.Twitter;
using TweetsGatherer.Exceptions;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.Services
{
    public class TwitterAuthorizationService : ITwitterAuthorizationService
    {
        private const string AuthEndpointSettingKey = "twitter:ApplicationAuthEndpoint";

        private readonly string _authEndpoint;

        private readonly IRestClient _restClient;

        public TwitterAuthorizationService(IAppSettings appSettings, IRestClient restClient)
        {
            _restClient = restClient;
            _authEndpoint = appSettings.Get(AuthEndpointSettingKey);
        }

        public string GetApplicationOnlyAccessToken(string consumerKey, string consumerSecret)
        {
            if (string.IsNullOrWhiteSpace(consumerKey))
                throw new ArgumentException("Parameter cannot be null or empty", "consumerKey");

            if (string.IsNullOrWhiteSpace(consumerSecret))
                throw new ArgumentException("Parameter cannot be null or empty", "consumerSecret");

            var consumerKeyAndSecret = "{0}:{1}".With(consumerKey, consumerSecret);

            var encodedConsumerKeyAndSecret = consumerKeyAndSecret.Base64Encode();

            var request = new RestRequest(_authEndpoint, Method.POST);
            request.AddHeader("Authorization", "Basic {0}".With(encodedConsumerKeyAndSecret));
            request.Parameters.Add(new Parameter { Name = "application/x-www-form-urlencoded", Value = "grant_type=client_credentials", Type = ParameterType.RequestBody });

            try
            {
                var response = _restClient.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<AuthTokenResponseDto>(response.Content).access_token;
                }
                throw new Exception("Twitter api response status code : {0} [Content: {1}]".With(response.StatusCode, response.Content));
            }
            catch (Exception ex)
            {
                throw new TwitterAuthorizationException("Rest call to twitter auth endpoint failed.", ex);
            }
        }
    }
}
