﻿using System;

namespace TweetsGatherer.Exceptions
{
    public class TwitterAuthorizationException : Exception
    {
        public TwitterAuthorizationException()
        {
        }

        public TwitterAuthorizationException(string message)
            : base(message)
        {
        }

        public TwitterAuthorizationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
