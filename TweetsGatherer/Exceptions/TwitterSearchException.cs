﻿using System;

namespace TweetsGatherer.Exceptions
{
    public class TwitterSearchException : Exception
    {
        public TwitterSearchException()
        {
        }

        public TwitterSearchException(string message)
            : base(message)
        {
        }

        public TwitterSearchException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
