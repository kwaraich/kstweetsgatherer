﻿using System.Collections.Generic;
using System.Linq;
using TweetsGatherer.Dtos;
using TweetsGatherer.Services;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.Tasks
{
    public class TweetsTask : ITweetsTask
    {
        private const string TwitterConsumerKeySettingKey = "twitter:ConsumerKey";
        private const string TwitterConsumerSecretSettingKey = "twitter:ConsumerSecret";
        private const string TwitterAccountsSettingKey = "TwitterAccounts";
        private const string NumberOfDaysSettingKey = "PastNumberOfDaysToGrabTweets";

        private readonly IAppSettings _appSettings;
        private readonly ITwitterAuthorizationService _twitterAuthorizationService;
        private readonly ITweetsSearchService _tweetsSearchService;

        public TweetsTask(IAppSettings appSettings, ITwitterAuthorizationService twitterAuthorizationService, ITweetsSearchService tweetsSearchService)
        {
            _appSettings = appSettings;
            _twitterAuthorizationService = twitterAuthorizationService;
            _tweetsSearchService = tweetsSearchService;
        }

        public ResponseDto GetTweets()
        {
            string consumerKey = _appSettings.Get(TwitterConsumerKeySettingKey);
            string consumerSecret = _appSettings.Get(TwitterConsumerSecretSettingKey);
            int numberOfDays = int.Parse(_appSettings.Get(NumberOfDaysSettingKey));
            string[] accounts = _appSettings.Get(TwitterAccountsSettingKey).Split(',');

            //Aquire access token
            var accessToken = _twitterAuthorizationService.GetApplicationOnlyAccessToken(consumerKey, consumerSecret);

            ResponseDto dto = new ResponseDto();

            List<TweetDto> tweetsList = new List<TweetDto>();
            List<AccountAggregateInfoDto> aggregateInfoList = new List<AccountAggregateInfoDto>();

            foreach (var account in accounts)
            {
                //Get tweets for this account
                var tweetsResult = _tweetsSearchService.GetTweets(account, numberOfDays, accessToken);

                //Create TweetsListDto for this account
                List<TweetDto> tweets = tweetsResult.Select(tweetResult =>
                            new TweetDto
                            {
                                Account = account,
                                Tweet = tweetResult.Content,
                                TweetDateAndTime = tweetResult.DateAndTime
                            }).ToList();

                int totalNumberOfUsersMentioned = tweetsResult.Sum(tweetResult => tweetResult.NumberOfUsersMentioned);

                //Create AggregateInfoListDto for this account
                AccountAggregateInfoDto aggregateInfo = new AccountAggregateInfoDto
                {
                    Account = account,
                    NumberOfTweets = tweetsResult.Count,
                    NumberOfUsersMentionedInTweets = totalNumberOfUsersMentioned,
                    Info = "The @{0} tweeted {1} times, in which {2} users were mentioned".With(account, tweetsResult.Count, totalNumberOfUsersMentioned)
                };

                //Add tweets from this account to the tweets dto list
                tweetsList.AddRange(tweets);

                //Add aggregate for this account info to dto list
                aggregateInfoList.Add(aggregateInfo);
            }

            //Order(ascending order by default) tweets by date&time and assign it to the response dto
            dto.TweetsData = tweetsList.OrderBy(x => x.TweetDateAndTime).ToArray();
            dto.AccountsAggregateInformation = aggregateInfoList.ToArray();

            return dto;
        }
    }
}
