﻿using TweetsGatherer.Dtos;

namespace TweetsGatherer.Tasks
{
    public interface ITweetsTask : ITask
    {
        ResponseDto GetTweets();
    }
}
