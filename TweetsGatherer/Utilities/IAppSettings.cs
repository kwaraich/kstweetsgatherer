﻿namespace TweetsGatherer.Utilities
{
    /// <summary>
    /// A wrapper around Application Settings to be implemented by the front end running application.
    /// </summary>
    public interface IAppSettings
    {
        string Get(string key);
    }
}
