﻿using StructureMap.Configuration.DSL;
using TweetsGatherer.Services;
using TweetsGatherer.Tasks;

namespace TweetsGatherer.IoC
{
    public class RegisterAll : Registry
    {
        public RegisterAll()
        {
            Scan(s =>
            {
                s.AssemblyContainingType<IService>();
                s.WithDefaultConventions();
                s.AddAllTypesOf<IService>();
                s.RegisterConcreteTypesAgainstTheFirstInterface();
            });

            Scan(s =>
            {
                s.AssemblyContainingType<ITask>();
                s.WithDefaultConventions();
                s.AddAllTypesOf<ITask>();
                s.RegisterConcreteTypesAgainstTheFirstInterface();
            });


        }
    }
}
