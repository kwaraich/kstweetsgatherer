﻿using RestSharp;
using StructureMap;
using StructureMap.Graph;

namespace TweetsGatherer.IoC
{
    public class StartupTask
    {
        public static void Initialize(IContainer container)
        {
            //Configure RestSharp.IRestClient to use RestSharp.RestClient
            container.Configure(x => x.For<IRestClient>().Use(new RestClient("https://api.twitter.com/")));

            container.Configure(c =>
            {
                c.Scan(s =>
                {
                    s.LookForRegistries();
                    s.TheCallingAssembly();
                });
            });


        }
    }
}
