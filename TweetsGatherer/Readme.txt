﻿Considering the scope of the project currently the Project TweetsGatherer contains following:
- Domain
- Dtos
- Services
- Tasks
- Utilities

All these root folders can be spereated into their respective projects for better code organization, like:
- TweetsGatherer.Domain
- TweetsGatherer.Dtos
- TweetsGatherer.Services
- TweetsGatherer.Tasks
- TweetsGatherer.Utilities