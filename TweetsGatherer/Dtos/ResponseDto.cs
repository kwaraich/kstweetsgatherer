﻿namespace TweetsGatherer.Dtos
{
    public class ResponseDto
    {
        public AccountAggregateInfoDto[] AccountsAggregateInformation { get; set; }
        public TweetDto[] TweetsData { get; set; }
    }
}
