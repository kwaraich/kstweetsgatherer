﻿namespace TweetsGatherer.Dtos.Twitter
{

    public class AuthTokenResponseDto
    {
        public string token_type { get; set; }
        public string access_token { get; set; }
    }
}
