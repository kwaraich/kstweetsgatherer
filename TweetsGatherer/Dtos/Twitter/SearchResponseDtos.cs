﻿namespace TweetsGatherer.Dtos.Twitter
{
    public class SearchResponseDto
    {
        public SearchResponseTweetDto[] statuses { get; set; }
    }

    public class SearchResponseTweetDto
    {
        public string created_at { get; set; }
        public string text { get; set; }
        public SearchResponseEntitiesDto entities { get; set; }
    }

    public class SearchResponseEntitiesDto
    {
        public SearchResponseUserDto[] user_mentions { get; set; }
    }

    public class SearchResponseUserDto
    {
        public string screen_name { get; set; }
        public string name { get; set; }
    }
}
