﻿using System;

namespace TweetsGatherer.Dtos
{
    public class TweetDto
    {
        public string Account { get; set; }
        public string Tweet { get; set; }
        public DateTime TweetDateAndTime { get; set; }
    }
}
