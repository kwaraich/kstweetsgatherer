﻿namespace TweetsGatherer.Dtos
{
    public class AccountAggregateInfoDto
    {
        public string Account { get; set; }
        public int NumberOfTweets { get; set; }
        public int NumberOfUsersMentionedInTweets { get; set; }
        public string Info { get; set; }
    }
}
