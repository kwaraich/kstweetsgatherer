﻿using System;

namespace TweetsGatherer.Domain
{
    public class Tweet
    {
        public string Content { get; set; }
        public DateTime DateAndTime { get; set; }
        public int NumberOfUsersMentioned { get; set; }
    }
}
