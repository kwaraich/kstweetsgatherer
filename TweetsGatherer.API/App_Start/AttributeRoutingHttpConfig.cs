using System.Web.Http;
using AttributeRouting.Web.Http.WebHost;
using TweetsGatherer.API.Controllers;

namespace TweetsGatherer.API 
{
    public static class AttributeRoutingHttpConfig
	{
		public static void RegisterRoutes(HttpRouteCollection routes) 
		{
            routes.MapHttpAttributeRoutes(config =>
            {
                config.UseLowercaseRoutes = true;
                config.AddRoutesFromAssemblyOf<DefaultController>();
                config.InMemory = true;
            });
		}

        public static void Start() 
		{
            RegisterRoutes(GlobalConfiguration.Configuration.Routes);
        }
    }
}
