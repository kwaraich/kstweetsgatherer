﻿using System.Web;
using System.Web.Http;
using StructureMap;
using StructureMap.Graph;
using TweetsGatherer.API.IoC;
using TweetsGatherer.API.Utilties;
using TweetsGatherer.IoC;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            //Configure Attribute Routing
            AttributeRoutingHttpConfig.RegisterRoutes(GlobalConfiguration.Configuration.Routes);

            //Configure  Dependency Injection
            IContainer container = ConfigureDependencies();
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }

        private static IContainer ConfigureDependencies()
        {
            var container = new Container(c =>
                c.Scan(s =>
                {
                    s.LookForRegistries();
                    s.TheCallingAssembly();
                }));

            //Configure the TweetsGatherer.IAppSettings to use the implementation from TweetsGatherer.Api.AppSettings
            container.Configure(x=>x.For<IAppSettings>().Use<AppSettings>());

            StartupTask.Initialize(container);


            return container;
        }
    }
}
