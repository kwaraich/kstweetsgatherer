﻿using System.Configuration;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.API.Utilties
{
    public class AppSettings : IAppSettings
    {
        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}