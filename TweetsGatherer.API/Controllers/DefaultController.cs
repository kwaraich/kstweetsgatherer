﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using TweetsGatherer.Exceptions;
using TweetsGatherer.Tasks;
using TweetsGatherer.Utilities;

namespace TweetsGatherer.API.Controllers
{
    public class DefaultController : ApiController
    {
        private readonly ITweetsTask _task;

        public DefaultController(ITweetsTask task)
        {
            _task = task;
        }

        [GET("")]
        public HttpResponseMessage GetDefaultMessage(HttpRequestMessage request)
        {
            return request.CreateResponse(HttpStatusCode.OK,
                "Use {root_url}/tweets to get tweets & specify approriate accept header value : application/xml or application/json");
        }

        [GET("tweets")]
        public HttpResponseMessage GetTweets(HttpRequestMessage request)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, _task.GetTweets());
            }
            //Custom Exceptions could be handled in a specific way if required, currently they are all being handled same way
            catch (TwitterAuthorizationException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, "Message : {0} [Full StackTrace : {1}]".With(ex.Message, ex.StackTrace));
            }
            catch (TwitterSearchException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, "Message : {0} [Full StackTrace : {1}]".With(ex.Message, ex.StackTrace));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, "Message : {0} [Full StackTrace : {1}]".With(ex.Message, ex.StackTrace));
            }
        }
    }
}
